﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTable
{
    public partial class PersonEntryForm : Form
    {
        public PersonEntry Entry { get; private set; }
        
        public PersonEntryForm(PersonEntry entry = null)
        {
            InitializeComponent();
            if (entry != null)
                InitFields(entry);
        }

        private void InitFields(PersonEntry entry)
        {
            NameTb.Text = entry.Person.Name;
            SurnameTb.Text = entry.Person.Surname;
            PatronymicTb.Text = entry.Person.Patronymic;

            RegionTb.Text = entry.Registration.Region;
            CityTb.Text = entry.Registration.City;
            StreetTb.Text = entry.Registration.Street;
            HouseTb.Text = entry.Registration.House;
            FlatTb.Text = entry.Registration.Flat;

            SeriesMtb.Text = entry.Passport.Series;
            NumberMtb.Text = entry.Passport.Number;
            DepartmentMtb.Text = entry.Passport.DepartmentCode;
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            if (ValidateFields() == false) 
            {
                MessageBox.Show("Не все поля заполнены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var name = new PersonName(NameTb.Text, SurnameTb.Text, PatronymicTb.Text);
            var address = new Address(RegionTb.Text, CityTb.Text, StreetTb.Text, HouseTb.Text, FlatTb.Text);
            var passport = new PassportData(SeriesMtb.Text, NumberMtb.Text, DepartmentMtb.Text);
            Entry = new PersonEntry(address, name, passport);
            DialogResult = DialogResult.OK;
            Close();
        }

        private bool ValidateFields()
        {   

            if (string.IsNullOrWhiteSpace(NameTb.Text) ||
                string.IsNullOrWhiteSpace(SurnameTb.Text) ||
                string.IsNullOrWhiteSpace(PatronymicTb.Text) ||
                string.IsNullOrWhiteSpace(RegionTb.Text) ||
                string.IsNullOrWhiteSpace(CityTb.Text) ||
                string.IsNullOrWhiteSpace(StreetTb.Text) ||
                string.IsNullOrWhiteSpace(HouseTb.Text) ||
                string.IsNullOrWhiteSpace(FlatTb.Text))
                return false;

            DepartmentMtb.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            var department = DepartmentMtb.Text;
            var series = SeriesMtb.Text;
            var number = NumberMtb.Text;

            if (department.Length < 6 || department.Contains(' ') ||
                series.Length < 4 || series.Contains(' ') ||
                number.Length < 6 || number.Contains(' '))
                return false;

            return true;
        }
    }
}
