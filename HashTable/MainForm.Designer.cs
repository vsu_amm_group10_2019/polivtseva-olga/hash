﻿
namespace HashTable
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.addTool = new System.Windows.Forms.ToolStripButton();
            this.updateTool = new System.Windows.Forms.ToolStripButton();
            this.findTool = new System.Windows.Forms.ToolStripButton();
            this.deleteTool = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveTool = new System.Windows.Forms.ToolStripButton();
            this.loadTool = new System.Windows.Forms.ToolStripButton();
            this.findUpdateTool = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView.RowTemplate.Height = 25;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(800, 450);
            this.dataGridView.TabIndex = 0;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Текстовые файлы|*.txt|Бинарные файлы|*.dat";
            this.saveFileDialog.RestoreDirectory = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "txt";
            this.openFileDialog.Filter = "Текстовые файлы|*.txt|Бинарные файлы|*.dat";
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTool,
            this.updateTool,
            this.findTool,
            this.deleteTool,
            this.findUpdateTool,
            this.toolStripSeparator1,
            this.saveTool,
            this.loadTool});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(800, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // addTool
            // 
            this.addTool.Name = "addTool";
            this.addTool.Size = new System.Drawing.Size(63, 22);
            this.addTool.Text = "Добавить";
            this.addTool.Click += new System.EventHandler(this.AddTool_Clicked);
            // 
            // updateTool
            // 
            this.updateTool.Name = "updateTool";
            this.updateTool.Size = new System.Drawing.Size(105, 22);
            this.updateTool.Text = "Отредактировать";
            this.updateTool.Click += new System.EventHandler(this.UpdateTool_Clicked);
            // 
            // findTool
            // 
            this.findTool.Name = "findTool";
            this.findTool.Size = new System.Drawing.Size(45, 22);
            this.findTool.Text = "Найти";
            this.findTool.Click += new System.EventHandler(this.FindTool_Clicked);
            // 
            // deleteTool
            // 
            this.deleteTool.Name = "deleteTool";
            this.deleteTool.Size = new System.Drawing.Size(55, 22);
            this.deleteTool.Text = "Удалить";
            this.deleteTool.Click += new System.EventHandler(this.DeleteTool_Clicked);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // saveTool
            // 
            this.saveTool.Name = "saveTool";
            this.saveTool.Size = new System.Drawing.Size(70, 22);
            this.saveTool.Text = "Сохранить";
            this.saveTool.Click += new System.EventHandler(this.SaveTool_Clicked);
            // 
            // loadTool
            // 
            this.loadTool.Name = "loadTool";
            this.loadTool.Size = new System.Drawing.Size(58, 22);
            this.loadTool.Text = "Загрузть";
            this.loadTool.Click += new System.EventHandler(this.LoadTool_Clicked);
            // 
            // findUpdateTool
            // 
            this.findUpdateTool.Name = "findUpdateTool";
            this.findUpdateTool.Size = new System.Drawing.Size(150, 22);
            this.findUpdateTool.Text = "Найти и отредактировать";
            this.findUpdateTool.Click += new System.EventHandler(this.FindUpdateTool_Clicked);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.dataGridView);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "MainForm";
            this.Text = "Хэш-таблица";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton addTool;
        private System.Windows.Forms.ToolStripButton updateTool;
        private System.Windows.Forms.ToolStripButton findTool;
        private System.Windows.Forms.ToolStripButton deleteTool;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton saveTool;
        private System.Windows.Forms.ToolStripButton loadTool;
        private System.Windows.Forms.ToolStripButton findUpdateTool;
    }
}

