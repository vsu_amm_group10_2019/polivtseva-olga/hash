﻿using System;

namespace HashTable
{
    [Serializable]
    public struct PersonName : IComparable<PersonName>
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Patronymic { get; private set; }

        public PersonName(string name, string surname, string patronymic)
        {
            Name = name;
            Surname = surname;
            Patronymic = patronymic;
        }

        public PersonName(string str)
        {
            str.Trim();
            int space = str.IndexOf(' ');
            int secondSpace = str.LastIndexOf(' ');
            Surname = str.Substring(0, space);
            Name = str.Substring(space + 1, secondSpace - space - 1);
            Patronymic = str.Substring(secondSpace + 1);
        }

        public override string ToString()
        {
            return $"{Surname} {Name} {Patronymic}";
        }

        public int CompareTo(PersonName other)
        {
            return this.ToString().CompareTo(other.ToString());
        }
    }
}
