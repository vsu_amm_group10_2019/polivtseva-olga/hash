﻿using System;

namespace HashTable
{
    [Serializable]
    public struct Address : IComparable<Address>
    {
        public string Region { get; private set; }
        public string City { get; private set; }
        public string Street { get; private set; }
        public string House { get; private set; }
        public string Flat { get; private set; }

        public Address(string Region, string City, string Street, string House, string Flat)
        {
            this.Region = Region;
            this.City = City;
            this.Street = Street;
            this.House = House;
            this.Flat = Flat;
        }

        public Address(string str)
        {
            int entryBeginig = 0;
            int comma = str.IndexOf(',');
            Region = str.Substring(entryBeginig, comma - entryBeginig);

            entryBeginig = comma + 2;
            comma = str.IndexOf(',', comma + 1);
            City = str.Substring(entryBeginig, comma - entryBeginig);

            entryBeginig = comma + 2;
            comma = str.IndexOf(',', comma + 1);
            Street = str.Substring(entryBeginig, comma - entryBeginig);

            entryBeginig = comma + 2;
            comma = str.IndexOf(',', comma + 1);
            House = str.Substring(entryBeginig, comma - entryBeginig);

            entryBeginig = comma + 2;
            Flat = str.Substring(entryBeginig);
        }

        public override string ToString()
        {
            return $"{Region}, {City}, {Street}, {House}, {Flat}";
        }

        public int CompareTo(Address other)
        {
            return this.ToString().CompareTo(other.ToString());
        }
    }
}
