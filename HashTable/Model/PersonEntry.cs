﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{

    [Serializable]
    public class PersonEntry : IComparable<PersonEntry>
    {
        public Address Registration { get; private set; }
        public PersonName Person { get; private set; }
        public PassportData Passport { get; private set; }

        public PersonEntry(Address registration, PersonName person, PassportData passport)
        {
            Registration = registration;
            Person = person;
            Passport = passport;
        }

        public PersonEntry(string str)
        {
            int colon = str.IndexOf(':');
            int semicolon = str.IndexOf(';');
            Passport = new PassportData(str.Substring(colon + 2, semicolon - colon - 2));

            colon = str.IndexOf(':', semicolon);
            semicolon = str.IndexOf(';', semicolon + 1);
            Person = new PersonName(str.Substring(colon + 2, semicolon - colon - 2));

            colon = str.IndexOf(':', semicolon);
            Registration = new Address(str.Substring(colon + 2));
        }

        public override string ToString()
        {
            return $"Паспорт: {Passport}; ФИО: {Person}; Адрес регистрации: {Registration}";
        }

        public int CompareTo(PersonEntry other)
        {
            return this.ToString().CompareTo(other.ToString());
        }

    }
}
