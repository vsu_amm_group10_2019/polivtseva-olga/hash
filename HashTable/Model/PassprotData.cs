﻿using System;

namespace HashTable
{
    [Serializable]
    public struct PassportData : IComparable<PassportData>
    {
        public string Number { get; private set; }
        public string DepartmentCode { get; private set; }
        public string Series { get; private set; }

        public PassportData(string Series, string Number, string DepartmentCode)
        {
            if ((long.TryParse(Series, out long _) &&
                long.TryParse(Number, out _) &&
                long.TryParse(DepartmentCode, out _)) == false)
                throw new ArgumentException();
            this.Series = Series;
            this.Number = Number;
            this.DepartmentCode = DepartmentCode;
        }


        public PassportData(string str)
        {
            var comma = str.IndexOf(',');
            var entryBeginig = 18;
            DepartmentCode = str.Substring(entryBeginig, 3) + str.Substring(entryBeginig + 4, 3);

            entryBeginig = comma + 2;
            Series = str.Substring(entryBeginig, 4);
            Number = str.Substring(entryBeginig + 5, 6);
        }

        public override string ToString()
        {
            return $"Код подразделения {DepartmentCode.Substring(0,3)}-{DepartmentCode.Substring(3)}, {Series} {Number}";
        }

        public int CompareTo(PassportData other)
        {
            return this.ToString().CompareTo(other.ToString());
        }
    }
}
