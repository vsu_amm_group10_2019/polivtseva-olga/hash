﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    public enum EntryState
    {
        Free,
        WithContent,
        Deleted,
    }

    public struct Cell
    {
        public PersonEntry entry;
        public EntryState state;
    }

    public class HashTable
    {
        public int MaxSize { get; private set; }
        public int StepSize { get; private set; }
        private int Count { get; set; }
        private Cell[] Table { get; }

        public HashTable(int maxSize = 101, int stepSize = 1)
        {
            MaxSize = maxSize;
            StepSize = stepSize;
            Table = new Cell[maxSize];
            Count = 0;
            InitTable();
        }

        private void InitTable()
        {
            for (int i = 0; i < MaxSize; i++)
                Table[i].state = EntryState.Free;
        }

        public static ulong GetKey(PassportData passport)
        {
            string pas = passport.Series + passport.Number + passport.DepartmentCode;
            return ulong.Parse(pas);
        }

        public int HashFunction(ulong key)
        {
            return (int)(key % (ulong)MaxSize);
        }

        private int NextCell(int firstStep, int currentStep)
        {
            return (firstStep + currentStep * StepSize) % MaxSize;
        }

        private int IndexOf(ulong key)
        {
            int firstIndex = HashFunction(key);
            int i = 0;
            int currentIndex = firstIndex;
            do
            {
                i++;
                switch (Table[currentIndex].state)
                {
                    case EntryState.Free:
                        return -1;
                    case EntryState.Deleted:
                        currentIndex = NextCell(firstIndex, i);
                        break;
                    case EntryState.WithContent:
                        if (key == GetKey(Table[currentIndex].entry.Passport))
                            return currentIndex;
                        else currentIndex = NextCell(firstIndex, i);
                        break;
                }
            }
            while (i < MaxSize);
            return -1;
        }

        public void Clear()
        {
            for (int i = 0; i < MaxSize; i++)
            {
                Table[i].state = EntryState.Free;
                Table[i].entry = null;
            }
            Count = 0;
        }

        public bool Add(PersonEntry entry)
        {
            ulong key = GetKey(entry.Passport);
            int index = IndexOf(key);
            if (Count == MaxSize || index != -1)
                return false;

            index = HashFunction(key);
            int i = 0;
            while (Table[index].state == EntryState.WithContent)
                index = NextCell(index, ++i);

            Table[index].entry = entry;
            Table[index].state = EntryState.WithContent;
            Count++;
            return true;
        }

        public bool Delete(ulong key)
        {
            int index = IndexOf(key);
            if (Count == 0 || index == -1)
                return false;

            Table[index].state = EntryState.Deleted;
            Count--;
            return true;
        }

        public PersonEntry Find(ulong key)
        {
            int index = IndexOf(key);
            if (Count == 0 || index == -1)
                return null;

            return Table[index].entry;
        }

        public PersonEntry Find(PassportData passport)
        {
            return Find(GetKey(passport));
        }

        public List<PersonEntry> ToList()
        {
            List<PersonEntry> result = new List<PersonEntry>();
            foreach (Cell cell in Table)
            {
                if (cell.state == EntryState.WithContent)
                    result.Add(cell.entry);
            }
            return result;
        }

        public void LoadFromList(List<PersonEntry> list)
        {
            foreach (PersonEntry record in list)
                Add(record);
        }
    }
}
