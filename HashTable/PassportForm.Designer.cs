﻿
namespace HashTable
{
    partial class PassportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.NumberMtb = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SeriesMtb = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DepartmentMtb = new System.Windows.Forms.MaskedTextBox();
            this.OKBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(11, 66);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 20);
            this.label11.TabIndex = 27;
            this.label11.Text = "Номер";
            // 
            // NumberMtb
            // 
            this.NumberMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.NumberMtb.Location = new System.Drawing.Point(183, 64);
            this.NumberMtb.Margin = new System.Windows.Forms.Padding(2);
            this.NumberMtb.Mask = "000000";
            this.NumberMtb.Name = "NumberMtb";
            this.NumberMtb.Size = new System.Drawing.Size(63, 24);
            this.NumberMtb.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(11, 37);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 20);
            this.label10.TabIndex = 25;
            this.label10.Text = "Серия";
            // 
            // SeriesMtb
            // 
            this.SeriesMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.SeriesMtb.Location = new System.Drawing.Point(183, 35);
            this.SeriesMtb.Margin = new System.Windows.Forms.Padding(2);
            this.SeriesMtb.Mask = "0000";
            this.SeriesMtb.Name = "SeriesMtb";
            this.SeriesMtb.Size = new System.Drawing.Size(63, 24);
            this.SeriesMtb.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(11, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 20);
            this.label9.TabIndex = 23;
            this.label9.Text = "Код подразделения";
            // 
            // DepartmentMtb
            // 
            this.DepartmentMtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.DepartmentMtb.Location = new System.Drawing.Point(183, 7);
            this.DepartmentMtb.Margin = new System.Windows.Forms.Padding(2);
            this.DepartmentMtb.Mask = "000-000";
            this.DepartmentMtb.Name = "DepartmentMtb";
            this.DepartmentMtb.Size = new System.Drawing.Size(63, 24);
            this.DepartmentMtb.TabIndex = 22;
            // 
            // OKBtn
            // 
            this.OKBtn.Location = new System.Drawing.Point(12, 106);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(102, 24);
            this.OKBtn.TabIndex = 29;
            this.OKBtn.Text = "Поиск";
            this.OKBtn.UseVisualStyleBackColor = true;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(135, 106);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(111, 24);
            this.CancelBtn.TabIndex = 28;
            this.CancelBtn.Text = "Отменить";
            this.CancelBtn.UseVisualStyleBackColor = true;
            // 
            // PassportForm
            // 
            this.AcceptButton = this.OKBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(258, 142);
            this.Controls.Add(this.OKBtn);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.NumberMtb);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.SeriesMtb);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DepartmentMtb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PassportForm";
            this.Text = "Пасспортные данные";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox NumberMtb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox SeriesMtb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox DepartmentMtb;
        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.Button CancelBtn;
    }
}