﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HashTable
{
    public partial class PassportForm : Form
    {
        public PassportData Passport { get; private set; }

        public PassportForm()
        {
            InitializeComponent();
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            if (ValidateFields() == false)
            {
                MessageBox.Show("Не все поля заполнены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Passport = new PassportData(SeriesMtb.Text, NumberMtb.Text, DepartmentMtb.Text);
            DialogResult = DialogResult.OK;
            Close();
        }

        private bool ValidateFields()
        {
            DepartmentMtb.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            var department = DepartmentMtb.Text;
            var series = SeriesMtb.Text;
            var number = NumberMtb.Text;

            if (department.Length < 6 || department.Contains(' ') ||
                series.Length < 4 || series.Contains(' ') ||
                number.Length < 6 || number.Contains(' '))
                return false;

            return true;
        }
    }
}
