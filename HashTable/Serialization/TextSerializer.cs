﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    class TextSerializer : ISerializer
    {
        public HashTable Load(FileInfo file)
        {
            List<PersonEntry> list = new List<PersonEntry>();
            using var reader = new StreamReader(file.FullName);
            
            string entry;
            while ((entry = reader.ReadLine()) != null)
                list.Add(new PersonEntry(entry));

            HashTable table = new HashTable();
            table.LoadFromList(list);
            return table;
        }

        public void Save(FileInfo file, HashTable table)
        {
            string entries = "";
            foreach (var entry in table.ToList())
                entries += entry.ToString() + Environment.NewLine;

            using var writer = new StreamWriter(file.FullName, false);
                writer.Write(entries);
        }
    }
}
