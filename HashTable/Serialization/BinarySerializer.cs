﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    class BinarySerializer : ISerializer
    {
        public HashTable Load(FileInfo file)
        {
            var formatter = new BinaryFormatter();
            using var stream = new FileStream(file.FullName, FileMode.Open);
            var entries = (List<PersonEntry>)formatter.Deserialize(stream);
            var table = new HashTable();
            table.LoadFromList(entries);
            return table;
        }

        public void Save(FileInfo file, HashTable table)
        {
            var formatter = new BinaryFormatter();
            using var stream = new FileStream(file.FullName, FileMode.Create);
                formatter.Serialize(stream, table.ToList());            
        }
    }
}
