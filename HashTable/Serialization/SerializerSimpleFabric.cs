﻿using System;
using System.IO;

namespace HashTable
{
    class SerializerSimpleFabric
    {
        public static ISerializer GetSerializer(FileInfo file)
        {
            return file.Extension switch
            {
                ".txt" => new TextSerializer(),
                ".dat" => new BinarySerializer(),
                _ => throw new Exception("Unknown file extension"),
            };
        }
    }
}
