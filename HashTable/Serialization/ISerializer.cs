﻿using System.IO;

namespace HashTable
{
    interface ISerializer
    {
        public void Save(FileInfo file, HashTable table);
        public HashTable Load(FileInfo file);
    }
}
