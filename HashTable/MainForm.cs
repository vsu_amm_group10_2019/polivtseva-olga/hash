﻿using System;
using System.IO;
using System.Windows.Forms;

namespace HashTable
{
    public partial class MainForm : Form
    {
        private readonly Random random = new Random();
        private HashTable table;

        public MainForm()
        {
            InitializeComponent();
            table = new HashTable();
            GenerateTestData();
            dataGridView.AutoGenerateColumns = true;
        }

        private void AddTool_Clicked(object sender, EventArgs e)
        {
            Add();
        }

        private void Add(PersonEntry entry = null)
        {
            var entryForm = new PersonEntryForm(entry);
            if (entryForm.ShowDialog() == DialogResult.OK)
            {
                if (table.Add(entryForm.Entry) == false)
                {
                    MessageBox.Show(
                        "Запись с таким паспортом уже существует",
                        "Ошибка",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    Add(entryForm.Entry);
                }
                else
                    RefreshGrid();
            }
        }

        private void UpdateTool_Clicked(object sender, EventArgs e)
        {
            var entry = GetSelectedEntry();
            if (entry == null)
                return;
            Update(entry);
        }

        private void FindTool_Clicked(object sender, EventArgs e)
        {
            var entry = Find();
            string mes = entry == null 
                ? "Запись с заданными паспортными данными не найдена" 
                : entry.ToString();

            MessageBox.Show(
                mes, 
                "Результаты поиска", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Information);
        }

        private void FindUpdateTool_Clicked(object sender, EventArgs e)
        {
            var entry = Find();
            if (entry == null)
                MessageBox.Show(
                    "Запись с заданными паспортными данными не найдена", 
                    "Результаты поиска", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            else
                Update(entry);
        }

        private void Update(PersonEntry entry)
        {
            var entryForm = new PersonEntryForm(entry);
            if (entryForm.ShowDialog() == DialogResult.OK)
            {
                var samePassportEntry = table.Find(entryForm.Entry.Passport);
                if (samePassportEntry != null && samePassportEntry != entry)
                {
                    MessageBox.Show(
                        "Запись с таким паспортом уже существует",
                        "Ошибка",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    Update(entry);
                    return;
                }
                table.Delete(HashTable.GetKey(entry.Passport));
                entry = entryForm.Entry;
                table.Add(entry);
                RefreshGrid();
            }
        }

        private PersonEntry Find()
        {
            var passportForm = new PassportForm();
            if (passportForm.ShowDialog() == DialogResult.Cancel)
                return null;
            var passport = passportForm.Passport;
            return table.Find(passport);
        }

        private void DeleteTool_Clicked(object sender, EventArgs e)
        {
            var entry = GetSelectedEntry();
            if (entry is null)
                return;
            table.Delete(HashTable.GetKey(entry.Passport));
            RefreshGrid();
        }

        private void SaveTool_Clicked(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            var file = new FileInfo(saveFileDialog.FileName);
            var saver = SerializerSimpleFabric.GetSerializer(file);
            saver.Save(file, table);
        }

        private void LoadTool_Clicked(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;

            var file = new FileInfo(openFileDialog.FileName);
            var loader = SerializerSimpleFabric.GetSerializer(file);
            table = loader.Load(file);

            RefreshGrid();
        }


        private PersonEntry GetSelectedEntry()
        {
            var entry = (PersonEntry)dataGridView.SelectedRows[0].DataBoundItem;
            if (entry == null)
                MessageBox.Show("Запись не выбрана", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return entry;
        }

        private void GenerateTestData(int count = 10)
        {
            for (int i = 0; i < count; i++)
                table.Add(RandomEntry());
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dataGridView.DataSource = table.ToList();
        }

        private PersonEntry RandomEntry()
        {
            var address = new Address(GetRandomString(), GetRandomString(), GetRandomString(), GetRandomString(1, 3), GetRandomString(1, 3));
            var person = new PersonName(GetRandomString(), GetRandomString(), GetRandomString());
            var passport = new PassportData(
                random.Next(1000, 10000).ToString(),
                random.Next(100000, 1000000).ToString(),
                random.Next(100000, 1000000).ToString());

            return new PersonEntry(address, person, passport);
        }

        private string GetRandomString(int minLenght = 1, int maxLenght = 10)
        {
            int lenght = random.Next(minLenght, maxLenght);
            string result = "";
            for (int i = 0; i < lenght; i++)
            {
                result += (char)random.Next((int)'a', (int)'z');
            }
            return result;
        }
    }
}
